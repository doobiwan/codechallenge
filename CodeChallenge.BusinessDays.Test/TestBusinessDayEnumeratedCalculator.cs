﻿using System;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using CodeChallenge.BusinessDays.PublicHolidays;

namespace CodeChallenge.BusinessDays.Test
{
    [TestClass]
    public class TestBusinessDayEnumeratedCalculator
    {
        [TestClass]
        public class TestCalculateBusinessDays
        {
            [TestMethod]
            public void CalculateBusinessDays_simple()
            {
                var fromTime = new DateTime(2014, 9, 01);
                var toTime = new DateTime(2014, 9, 30);
                const int publicHolidays = 0;
                const int expected = 20;

                var mockHolidays = new Mock<IPublicHolidayProvider>();
                mockHolidays
                    .Setup(m => m.HolidaysBetween(It.IsAny<DateTime>(), It.IsAny<DateTime>()))
                    .Returns(publicHolidays);

                IBusinessDayCalculator calc = new BusinessDaysEnumeratedCalculator(mockHolidays.Object);

                var result = calc.CalculateBusinessDays(fromTime, toTime);

                result.Should().Be(expected, "because there should be {0} business days between {1} and {2}", expected, fromTime, toTime);
            }

            [TestMethod]
            public void CalculateBusinessDays_leading_weekend()
            {
                var fromTime = new DateTime(2014, 8, 31);
                var toTime = new DateTime(2014, 9, 27);
                const int publicHolidays = 0;
                const int expected = 20;

                var mockHolidays = new Mock<IPublicHolidayProvider>();
                mockHolidays
                    .Setup(m => m.HolidaysBetween(It.IsAny<DateTime>(), It.IsAny<DateTime>()))
                    .Returns(publicHolidays);

                IBusinessDayCalculator calc = new BusinessDaysEnumeratedCalculator(mockHolidays.Object);

                var result = calc.CalculateBusinessDays(fromTime, toTime);

                result.Should().Be(expected, "because there should be {0} business days between {1} and {2}", expected, fromTime, toTime);
            }

            [TestMethod]
            public void CalculateBusinessDays_multi_month()
            {
                var fromTime = new DateTime(2014, 7, 6);
                var toTime = new DateTime(2014, 9, 22);
                const int publicHolidays = 0;
                const int expected = 55;

                var mockHolidays = new Mock<IPublicHolidayProvider>();
                mockHolidays
                    .Setup(m => m.HolidaysBetween(It.IsAny<DateTime>(), It.IsAny<DateTime>()))
                    .Returns(publicHolidays);

                IBusinessDayCalculator calc = new BusinessDaysEnumeratedCalculator(mockHolidays.Object);

                var result = calc.CalculateBusinessDays(fromTime, toTime);

                result.Should().Be(expected, "because there should be {0} business days between {1} and {2}", expected, fromTime, toTime);
            }

            [TestMethod]
            public void CalculateBusinessDays_Task2_case_1()
            {
                var fromTime = new DateTime(2014, 8, 7);
                var toTime = new DateTime(2014, 8, 11);
                const int publicHolidays = 0;
                const int expected = 1;

                var mockHolidays = new Mock<IPublicHolidayProvider>();
                mockHolidays
                    .Setup(m => m.HolidaysBetween(It.IsAny<DateTime>(), It.IsAny<DateTime>()))
                    .Returns(publicHolidays);

                IBusinessDayCalculator calc = new BusinessDaysEnumeratedCalculator(mockHolidays.Object);

                var result = calc.CalculateBusinessDays(fromTime, toTime);

                result.Should().Be(expected, "because there should be {0} business days between {1} and {2}", expected, fromTime, toTime);
            }

            [TestMethod]
            public void CalculateBusinessDays_Task2_case_2()
            {
                var fromTime = new DateTime(2014, 8, 13);
                var toTime = new DateTime(2014, 8, 21);
                const int publicHolidays = 0;
                const int expected = 5;

                var mockHolidays = new Mock<IPublicHolidayProvider>();
                mockHolidays
                    .Setup(m => m.HolidaysBetween(It.IsAny<DateTime>(), It.IsAny<DateTime>()))
                    .Returns(publicHolidays);

                IBusinessDayCalculator calc = new BusinessDaysEnumeratedCalculator(mockHolidays.Object);

                var result = calc.CalculateBusinessDays(fromTime, toTime);

                result.Should().Be(expected, "because there should be {0} business days between {1} and {2}", expected, fromTime, toTime);
            }
        }
    }
}
