﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CodeChallenge.BusinessDays.PublicHolidays;
using FluentAssertions;
using CodeChallenge.BusinessDays.PublicHolidays.DynamicHolidays;
using System.Collections.Generic;

namespace CodeChallenge.BusinessDays.Test
{
    [TestClass]
    public class TestPublicHolidayProvider
    {
        [TestClass]
        public class TestStaticPublicHolidayProvider
        {
            [TestMethod]
            public void HolidaysBetween_AnzacDay_Saturday()
            {
                var fromTime = new DateTime(2015, 4, 20);
                var toTime = new DateTime(2015, 4, 28);
                const int expected = 0;

                IPublicHolidayProvider provider = new StaticPublicHolidayProvider();
                var result = provider.HolidaysBetween(fromTime, toTime);

                result.Should().Be(expected);
            }

            [TestMethod]
            public void HolidaysBetween_December_2014()
            {
                var fromTime = new DateTime(2014, 12, 12);
                var toTime = new DateTime(2014, 12, 31);
                const int expected = 2;

                IPublicHolidayProvider provider = new StaticPublicHolidayProvider();
                var result = provider.HolidaysBetween(fromTime, toTime);

                result.Should().Be(expected);
            }

            [TestMethod]
            public void HolidaysBetween_December_2015()
            {
                var fromTime = new DateTime(2015, 12, 12);
                var toTime = new DateTime(2015, 12, 31);
                const int expected = 1;

                IPublicHolidayProvider provider = new StaticPublicHolidayProvider();
                var result = provider.HolidaysBetween(fromTime, toTime);

                result.Should().Be(expected);
            }
        }

        [TestClass]
        public class TestDynamicPublicHolidayProvider
        {
            [TestMethod]
            public void HolidaysBetween_2015()
            {
                var fromTime = new DateTime(2014, 12, 31);
                var toTime = new DateTime(2015, 12, 31);
                const int expected = 5;

                var newYears = new NextWorkingDayPublicHoliday(1, 1);
                var anzacDay = new AlwaysExactDatePublicHoliday(25, 04);
                var queensBirthday = new DayWeekofMonthPublicHoliday(DayOfWeek.Monday, 2, 06);
                var LabourDay = new DayWeekofMonthPublicHoliday(DayOfWeek.Monday, 1, 06);
                var xmasDay = new NextWorkingDayPublicHoliday(25, 12);
                var boxingDay = new NextWorkingDayPublicHoliday(26, 12);
                

                IPublicHolidayProvider provider = new DynamicPublicHolidayProvider(new List<IDynamicHoliday>
                    {
                        newYears,
                        anzacDay,
                        queensBirthday,
                        LabourDay,
                        xmasDay,
                        boxingDay
                    });

                var result = provider.HolidaysBetween(fromTime, toTime);

                result.Should().Be(expected);
            }

            [TestMethod]
            public void HolidaysBetween_fin_year_2016()
            {
                var fromTime = new DateTime(2015, 6, 30);
                var toTime = new DateTime(2016, 6, 30);
                const int expected = 6;

                var newYears = new NextWorkingDayPublicHoliday(1, 1);
                var anzacDay = new AlwaysExactDatePublicHoliday(25, 04);
                var queensBirthday = new DayWeekofMonthPublicHoliday(DayOfWeek.Monday, 2, 06);
                var LabourDay = new DayWeekofMonthPublicHoliday(DayOfWeek.Monday, 1, 06);
                var xmasDay = new NextWorkingDayPublicHoliday(25, 12);
                var boxingDay = new NextWorkingDayPublicHoliday(26, 12);


                IPublicHolidayProvider provider = new DynamicPublicHolidayProvider(new List<IDynamicHoliday>
                    {
                        newYears,
                        anzacDay,
                        queensBirthday,
                        LabourDay,
                        xmasDay,
                        boxingDay
                    });

                var result = provider.HolidaysBetween(fromTime, toTime);

                result.Should().Be(expected);
            }
        }
    }
}
