﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CodeChallenge.BusinessDays.PublicHolidays.DynamicHolidays;
using FluentAssertions;

namespace CodeChallenge.BusinessDays.Test
{
    [TestClass]
    public class TestDynamicHoliday
    {
        [TestClass]
        public class TestGetInstanceInYear
        {
            [TestMethod]
            public void GetInstanceInYear_AlwaysExactDatePublicHoliday()
            {
                IDynamicHoliday publicHoliday = new AlwaysExactDatePublicHoliday(25, 04);

                var result2014 = publicHoliday.GetInstanceInYear(2014);
                var result2015 = publicHoliday.GetInstanceInYear(2015);

                result2014.DayOfWeek.Should().Be(DayOfWeek.Friday);
                result2015.DayOfWeek.Should().Be(DayOfWeek.Saturday);
            }

            [TestMethod]
            public void GetInstanceInYear_NextWorkingDayPublicHoliday()
            {
                IDynamicHoliday publicHoliday = new NextWorkingDayPublicHoliday(25, 04);

                var result2015 = publicHoliday.GetInstanceInYear(2015);

                result2015.DayOfWeek.Should().Be(DayOfWeek.Monday);
            }

            [TestMethod]
            public void GetInstanceInYear_DayWeekofMonthPublicHoliday()
            {
                IDynamicHoliday publicHoliday = new DayWeekofMonthPublicHoliday(DayOfWeek.Monday, 2, 06);

                var result2016 = publicHoliday.GetInstanceInYear(2016);

                result2016.Day.Should().Be(13);
            }
        }
    }
}
