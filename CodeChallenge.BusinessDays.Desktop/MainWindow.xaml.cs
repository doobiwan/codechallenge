﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Autofac;
using CodeChallenge.BusinessDays.PublicHolidays;

namespace CodeChallenge.BusinessDays.Desktop
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private IContainer container;

        public MainWindow()
        {
            InitializeComponent();

            container = BuildContainer();
        }

        private void btnCalculate_Click(object sender, RoutedEventArgs e)
        {
            if (dtFrom.SelectedDate == null || dtTo.SelectedDate == null)
            {
                MessageBox.Show("Please select the from and to dates.");
                return;
            }

            var calculator = container.Resolve<IBusinessDayCalculator>();
            try
            {

                txtDays.Text = string.Format("{0} days", calculator.CalculateBusinessDays(dtFrom.SelectedDate.Value, dtTo.SelectedDate.Value));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private IContainer BuildContainer()
        {
            var builder = new ContainerBuilder();

            builder.RegisterType<DynamicPublicHolidayProvider>().As<IPublicHolidayProvider>();
            builder.RegisterType<BusinessDaysEnumeratedCalculator>().As<IBusinessDayCalculator>();

            return builder.Build();
        }
    }
}
