﻿using CodeChallenge.BusinessDays.PublicHolidays;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeChallenge.BusinessDays
{
    public class BusinessDaysEnumeratedCalculator : IBusinessDayCalculator
    {
        private readonly IPublicHolidayProvider publicHolidays;

        public BusinessDaysEnumeratedCalculator(IPublicHolidayProvider publicHolidays)
        {
            this.publicHolidays = publicHolidays;
        }
        public int CalculateBusinessDays(DateTime from, DateTime to, bool inclusive = false)
        {
            //From http://stackoverflow.com/questions/1617049/calculate-the-number-of-business-days-between-two-dates
            var dayDifference = (int)to.Subtract(from).TotalDays - ((inclusive) ? 0 : 1);
            var days = Enumerable
                                .Range(1, dayDifference)
                                .Select(x => from.AddDays(x))
                                .Count(x => x.DayOfWeek != DayOfWeek.Saturday && x.DayOfWeek != DayOfWeek.Sunday);

            return days - publicHolidays.HolidaysBetween(from, to);
        }
    }
}
