﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeChallenge.BusinessDays.PublicHolidays.DynamicHolidays
{
    public class AlwaysExactDatePublicHoliday : IDynamicHoliday
    {
        private int day;
        private int month;

        public AlwaysExactDatePublicHoliday(int day, int month)
        {
            this.day = day;
            this.month = month;
        }
        public DateTime GetInstanceInYear(int year)
        {
            return new DateTime(year, month, day);
        }
    }
}
