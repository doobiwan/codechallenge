﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeChallenge.BusinessDays.PublicHolidays.DynamicHolidays
{
    public class DayWeekofMonthPublicHoliday : IDynamicHoliday
    {
        private DayOfWeek dayOfWeek;
        private int weekOfMonth;
        private int month;

        public DayWeekofMonthPublicHoliday(DayOfWeek dayOfWeek, int weekOfMonth, int month)
        {
            this.dayOfWeek = dayOfWeek;
            this.weekOfMonth = weekOfMonth;
            this.month = month;
        }

        public DateTime GetInstanceInYear(int year)
        {
            var start = new DateTime(year, month, 1);

            while (start.DayOfWeek != dayOfWeek)
                start += TimeSpan.FromDays(1);

            start += TimeSpan.FromDays(7 * (weekOfMonth - 1));

            return start;
        }
    }
}
