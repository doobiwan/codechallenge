﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeChallenge.BusinessDays.PublicHolidays.DynamicHolidays
{
    public interface IDynamicHoliday
    {
        DateTime GetInstanceInYear(int year);
    }
}
