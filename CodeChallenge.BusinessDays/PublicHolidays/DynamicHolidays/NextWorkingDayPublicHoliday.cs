﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeChallenge.BusinessDays.PublicHolidays.DynamicHolidays
{
    public class NextWorkingDayPublicHoliday : IDynamicHoliday
    {
        private int day;
        private int month;

        public NextWorkingDayPublicHoliday(int day, int month)
        {
            this.day = day;
            this.month = month;
        }

        public DateTime GetInstanceInYear(int year)
        {
            var resultDate = new DateTime(year, month, day);

            if (resultDate.DayOfWeek == DayOfWeek.Saturday)
                resultDate = resultDate + TimeSpan.FromDays(2);
            else if (resultDate.DayOfWeek == DayOfWeek.Sunday)
                resultDate = resultDate + TimeSpan.FromDays(1);

            return resultDate;
        }
    }
}
