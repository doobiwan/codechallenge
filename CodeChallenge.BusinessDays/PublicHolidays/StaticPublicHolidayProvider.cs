﻿using System;
using System.Collections.Generic;

namespace CodeChallenge.BusinessDays.PublicHolidays
{
    public class StaticPublicHolidayProvider : IPublicHolidayProvider
    {
        public int HolidaysBetween(DateTime from, DateTime to)
        {
            int excludedDates = 0;

            foreach (var holiday in publicHolidayDates)
            {
                int year = from.Year;
                DateTime publicHolidayDate = new DateTime(year, holiday.Item2, holiday.Item1);

                while (from < publicHolidayDate && publicHolidayDate < to)
                {
                    if (publicHolidayDate.DayOfWeek != DayOfWeek.Saturday && publicHolidayDate.DayOfWeek != DayOfWeek.Sunday)
                        excludedDates++;

                    publicHolidayDate = new DateTime(++year, holiday.Item2, holiday.Item1);
                }
            }

            return excludedDates;
        }

        private IEnumerable<Tuple<int, int>> publicHolidayDates = new List<Tuple<int, int>>
        {
            Tuple.Create(1,1),
            Tuple.Create(26,1),
            Tuple.Create(25,4),
            Tuple.Create(25,12),
            Tuple.Create(26,12)
        };
    }
}