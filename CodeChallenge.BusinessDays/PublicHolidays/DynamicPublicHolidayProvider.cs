﻿using CodeChallenge.BusinessDays.PublicHolidays.DynamicHolidays;
using System;
using System.Collections.Generic;

namespace CodeChallenge.BusinessDays.PublicHolidays
{
    public class DynamicPublicHolidayProvider : IPublicHolidayProvider
    {
        private List<IDynamicHoliday> publicHolidayDates;

        public DynamicPublicHolidayProvider(IEnumerable<IDynamicHoliday> holidays = null)
        {
            this.publicHolidayDates = (holidays == null)
                                ? new List<IDynamicHoliday>()
                                : new List<IDynamicHoliday>(holidays);
        }

        public int HolidaysBetween(DateTime from, DateTime to)
        {
            int excludedDates = 0;

            foreach (var holiday in publicHolidayDates)
            {
                int year = from.Year;
                DateTime publicHolidayDate = holiday.GetInstanceInYear(year);

                while (publicHolidayDate < to)
                {
                    if (from < publicHolidayDate && 
                        publicHolidayDate.DayOfWeek != DayOfWeek.Saturday && 
                        publicHolidayDate.DayOfWeek != DayOfWeek.Sunday)
                        excludedDates++;

                    publicHolidayDate = holiday.GetInstanceInYear(++year);
                }
            }

            return excludedDates;
        }

        public void AddHoliday(IDynamicHoliday holiday)
        {
            publicHolidayDates.Add(holiday);
        }
    }
}