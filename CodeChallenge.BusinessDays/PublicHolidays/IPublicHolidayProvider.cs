﻿using System;

namespace CodeChallenge.BusinessDays.PublicHolidays
{
    public interface IPublicHolidayProvider
    {
        int HolidaysBetween(DateTime from, DateTime to);
    }
}