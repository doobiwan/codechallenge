﻿using CodeChallenge.BusinessDays.PublicHolidays;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeChallenge.BusinessDays
{
    public class BusinessDayDivisiveCalculator : IBusinessDayCalculator
    {
        private readonly IPublicHolidayProvider publicHolidays;

        public BusinessDayDivisiveCalculator(IPublicHolidayProvider publicHolidays)
        {
            this.publicHolidays = publicHolidays;
        }

        public int CalculateBusinessDays(DateTime from, DateTime to, bool inclusive = false)
        {
            if (from > to)
                throw new ArgumentException("'from' must be before 'to'");

            int holidays = publicHolidays.HolidaysBetween(from, to);
            var first = (inclusive ? from : from + TimeSpan.FromDays(1)).Date;
            var last = (inclusive ? to : to - TimeSpan.FromDays(1)).Date;

            var fromTrim = 0;
            var toTrim = 0;

            if (first.DayOfWeek == DayOfWeek.Saturday)
                fromTrim = 2;
            if (first.DayOfWeek == DayOfWeek.Sunday)
                fromTrim = 1;

            if (last.DayOfWeek == DayOfWeek.Saturday)
                toTrim = 1;
            if (last.DayOfWeek == DayOfWeek.Sunday)
                toTrim = 2;

            var dateRange = (last - TimeSpan.FromDays(toTrim)) - (first + TimeSpan.FromDays(fromTrim));
            var totalDaysBetween = dateRange.Days + 1; //Mathematical offset

            int daysOver;
            var weeks = Math.DivRem(totalDaysBetween, 7, out daysOver);

            var days = weeks * 5;

            var result = (days + daysOver) - holidays;

            return result;

        }
    }
}
