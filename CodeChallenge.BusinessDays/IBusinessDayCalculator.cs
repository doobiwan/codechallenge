﻿using System;
namespace CodeChallenge.BusinessDays
{
    public interface IBusinessDayCalculator
    {
        int CalculateBusinessDays(DateTime from, DateTime to, bool inclusive = false);
    }
}
