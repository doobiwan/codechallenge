﻿using System;
using System.Collections.Generic;

namespace CodeChallenge.NameSearch
{
    public interface ISearchable
    {
        IEnumerable<string> Find(string segment);
    }
}
