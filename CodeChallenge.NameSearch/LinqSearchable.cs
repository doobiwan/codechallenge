﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeChallenge.NameSearch
{
    public class LinqSearchable : ISearchable
    {
        private readonly string[] source;

        public LinqSearchable(IEnumerable<string> source)
        {
            this.source = source.ToArray();
        }
        public IEnumerable<string> Find(string segment)
        {
            return source.Where(s => s.IndexOf(segment, StringComparison.InvariantCultureIgnoreCase) > -1).ToArray();
        }
    }
}
