﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeChallenge.NameSearch
{
    public class NaiveSearchable : ISearchable
    {
        private readonly string[] source;

        public NaiveSearchable(IEnumerable<string> source)
        {
            this.source = source.ToArray();
        }

        public IEnumerable<string> Find(string segment)
        {
            if (string.IsNullOrWhiteSpace(segment))
                throw new InvalidEnumArgumentException("segment must be non-empty");

            if (source == null || !source.Any())
                throw new InvalidDataException("Source recordset is empty");

            var hits = new List<string>();

            for (int i = 0; i < source.Count(); i++)
                if (source[i].IndexOf(segment, StringComparison.InvariantCultureIgnoreCase) > -1)
                    hits.Add(source[i]);

            return hits;
        }
    }
}
