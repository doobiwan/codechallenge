﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CodeChallenge.NameSearch;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Configuration;
namespace CodeChallenge.NameSearch.Tests
{
    [TestClass]
    public class TestLinqSearchable
    {
        [TestClass]
        public class FindTest
        {
            string[] source;

            [TestInitialize]
            public void Initialize()
            {
                var sourceLocation = ConfigurationManager.AppSettings["sourceLocation"];
                source = File.ReadAllLines(sourceLocation);
            }

            [TestMethod]
            public void Find_simple()
            {
                const string testCase = "Johnson";
                const int expected = 110;

                ISearchable searchable = new LinqSearchable(source);

                var sw = Stopwatch.StartNew();
                var result = searchable.Find(testCase);
                sw.Stop();

                Debug.WriteLine(sw.Elapsed);
                result.Should().HaveCount(expected, "because time should be less than {0}", sw.Elapsed);
            }

            [TestMethod]
            public void Find_case_insensitive()
            {
                var testCase1 = "Johnson";
                var testCase2 = "johNson";
                var expected = 110;

                ISearchable searchable = new LinqSearchable(source);

                var result1 = searchable.Find(testCase1);
                var result2 = searchable.Find(testCase2);

                result1.Should().HaveCount(expected);
                result2.Should().HaveCount(expected);
            }
        }
    }
}