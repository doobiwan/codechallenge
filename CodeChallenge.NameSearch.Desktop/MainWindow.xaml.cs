﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Autofac;
using Autofac.Core;
using IContainer = Autofac.IContainer;
using Microsoft.Win32;

namespace CodeChallenge.NameSearch.Desktop
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private IContainer container;
        private string[] source;
        private string segment;
        private IEnumerable<string> result;

        public MainWindow()
        {
            InitializeComponent();

            container = BuildContainer();
        }

        private async void txtSource_KeyUp(object sender, KeyEventArgs e)
        {
            if (source == null)
            {
                LoadSource();
                return;
            }

            if (string.IsNullOrWhiteSpace(txtSource.Text))
                return;

            ISearchable search = (chkType.IsChecked.Value)
                                        ? container.ResolveNamed<ISearchable>("linq")
                                        : container.ResolveNamed<ISearchable>("naive");

            segment = txtSource.Text;

            var sw = Stopwatch.StartNew();

            result = await FindAsync(search, segment);
            sw.Stop();

            txtOutput.Text = string.Format("Elapsed: {0}{2}Count: {1}{2}", sw.Elapsed, result.Count(), Environment.NewLine);

            var distinct = result.Distinct();

            if (distinct.Count() < 100)
                distinct.ToList().ForEach(s => txtOutput.Text += s + Environment.NewLine);
        }

        private void btnSource_Click(object sender, RoutedEventArgs e)
        {
            LoadSource();
        }

        private void LoadSource()
        {
            var dialogue = new OpenFileDialog
            {
                Title = "Select source file"
            };

            if (dialogue.ShowDialog(this).Value)
            {
                try
                {
                    source = File.ReadAllLines(dialogue.FileName);
                }
                catch (Exception)
                {
                    MessageBox.Show("please select a valid csv file");
                }

            }
        }

        private Task<IEnumerable<string>> FindAsync(ISearchable search, string segment)
        {
            return Task.Factory.StartNew(() => search.Find(segment));
        }

        private IContainer BuildContainer()
        {
            var builder = new ContainerBuilder();

            builder.Register(c => new LinqSearchable(source))
                   .Named<ISearchable>("linq");

            builder.Register(c => new NaiveSearchable(source))
                    .Named<ISearchable>("naive");

            return builder.Build();
        }
    }
}
